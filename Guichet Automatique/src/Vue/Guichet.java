package Vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Modele.Compte;

public class Guichet extends JFrame implements ActionListener {
	//	donnees statiques
	private static final long serialVersionUID = 1;
	private static final String[] nomsBoutons = { "Depot", "Retrait", "Quitter" };
	
	//	getter statique pour les noms des boutons
	public static String[] getNomsBoutons() {
		return nomsBoutons;
	}
	
	
	//	donnees d'instance
	private Compte leCompte;
	
	private JTextField txtfld_montant;
	private JLabel lbl_solde;

	//	constructeur
	public Guichet(String titre, Compte unCompte) {
		super(titre);

		this.leCompte = unCompte;

		// preparation de la fenetre
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 400);
		setLocation(1600, 300);

		// placement des objets graphiques
		setLayout(new GridLayout(7, 1));

		getContentPane().setBackground(new Color(0, 201, 172));

		// etiquette pour le solde
		add(new JLabel("Solde"));

		// etiquette pour le montant du solde
		String solde = String.valueOf(leCompte.getSolde());
		lbl_solde = new JLabel(solde);
		add(lbl_solde);

		// zone de saisie de texte pour le montant
		txtfld_montant = new JTextField();
		txtfld_montant.setBackground(new Color(0, 253, 253));
		add(txtfld_montant);

		// ajout des boutons
		
		//	avec une classe d'ecouteur specifique
		EcouteurBoutons monEcouteur = new EcouteurBoutons(this);
		
		for (String toto : nomsBoutons) {
			JButton bouton = new JButton(toto);
			
			//	avec une classe d'ecouteur specifique
			bouton.addActionListener(monEcouteur);
			
			//	OU
			//	utilisation du JFrame comme ecouteur
			// bouton.addActionListener(this);
			
			add(bouton);
		}

		// status de la transaction
		add(new JLabel("Aucune transaction effectuee"));

		// affichage de la fenetre
		setVisible(true);
	}

	
	//	getter pour les donnees d'instance
	public Compte getLeCompte() {
		return leCompte;
	}

	public JTextField getTxtfld_montant() {
		return txtfld_montant;
	}

	public JLabel getLbl_solde() {
		return lbl_solde;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		String txt = btn.getText();
		System.out.println("bouton : " + txt);

		if (txt == nomsBoutons[2]) {	//	on quitte l'application
			this.dispose();
		}
		
		// on recupere le montant du depot
		double montant = Double.parseDouble(txtfld_montant.getText());

		if (txt == nomsBoutons[0]) {
			// on effectue le depot
			this.leCompte.deposer(montant);
		} else if (txt == nomsBoutons[1]) {
			// on effectue le retrait
			this.leCompte.retirer(montant);
		} 

		// on demande au compte la valeur du nouveau solde apres depot
		String solde = String.valueOf(leCompte.getSolde());

		// on met a jour l'affichage du solde du compte
		lbl_solde.setText(solde);
	}
}






//------------------------------------
//	avec un classe specifique d'ecouteur
//	------------------------------------
class EcouteurBoutons implements ActionListener {
	private Guichet leGuichet;
	
	//	constructeur
	public EcouteurBoutons(Guichet leGuichet) {
		super();
		this.leGuichet = leGuichet;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		String txt = btn.getText();
		System.out.println("bouton : " + txt);

		if (txt == Guichet.getNomsBoutons()[2]) {	//	on quitte l'application
			leGuichet.dispose();
		}
		
		// on recupere le montant du depot
		double montant = Double.parseDouble(leGuichet.getTxtfld_montant().getText());

		if (txt == Guichet.getNomsBoutons()[0]) {
			// on effectue le depot
			leGuichet.getLeCompte().deposer(montant);
		} else if (txt == Guichet.getNomsBoutons()[1]) {
			// on effectue le retrait
			this.leGuichet.getLeCompte().retirer(montant);
		} 

		// on demande au compte la valeur du nouveau solde apres depot
		String solde = String.valueOf(leGuichet.getLeCompte().getSolde());

		// on met a jour l'affichage du solde du compte
		leGuichet.getLbl_solde().setText(solde);
	}

}