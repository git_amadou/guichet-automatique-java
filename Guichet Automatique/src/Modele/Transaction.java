package Modele;

import java.util.Calendar;
import java.util.Date;

enum TypeOperation {
	Retrait, depot
}

public class Transaction {
	private TypeOperation typeOperation;
	private double montant;
	private Date date;
	
	public Transaction(TypeOperation type, double montant) {
		this.typeOperation = type;
		this.montant = montant;
		this.date = Calendar.getInstance().getTime();
	}
}
