import Modele.Compte;
import Vue.Guichet;

public class Principale {
	public static void main(String[] args) {
		Compte unCompte = new Compte(500);
		
		new Guichet("GAB", unCompte);
		System.out.println("fin du main");
	}
}
