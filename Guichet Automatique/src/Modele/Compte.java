package Modele;

public class Compte {
	//	attributs
	private double solde;
	
	public Compte(double soldeInitial) {
		solde = soldeInitial;
	}
	
	//	getters/setters
	public double getSolde() {
		return solde;
	}
	
	//	methodes specifiques
	public void deposer(double montant) {
		solde += montant;
	}
	
	public void retirer(double montant) {
		if (solde >= montant) {
			solde -= montant;
		}
	}
}
